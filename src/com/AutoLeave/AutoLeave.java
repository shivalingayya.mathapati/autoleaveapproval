package com.AutoLeave;

import java.text.SimpleDateFormat;
import java.util.*;

public class AutoLeave {
	
	public static ArrayList<Employee> emps = new ArrayList<>();
	
	public static void main(String[] args) {
		
		GenerateEmployeeData();
		Scanner input = new Scanner(System.in);
		Long EmpID = 0L;
		String cond = null;
		SimpleDateFormat FromDate;
		SimpleDateFormat ToDate;
		LeaveInfo leaveInfo = new LeaveInfo();
		//LeaveStatus leaveStatus = LeaveStatus.Rejected;
		try 
		{
			System.out.println("Enter the employee ID : \n");
			String id = input.nextLine();
			EmpID = Long.parseLong(id);
			System.out.println("\nEnter from date in format DD/MM/YYYY\n");
			String inputFromdate = input.nextLine();
			FromDate = new SimpleDateFormat("dd/MM/yyyy");
			Date fromdate = FromDate.parse(inputFromdate);
			
			System.out.println("\nEnter to date in format DD/MM/YYYY\n");
			String inputTodate = input.nextLine();
			ToDate = new SimpleDateFormat("dd/MM/yyyy");
			Date todate = ToDate.parse(inputTodate);
			
			Long totalDays = leaveInfo.CalculateTotalNumberOfDays(fromdate, todate);
			
			System.out.println(totalDays.toString());
			
			LeaveStatus leaveStatus = leaveInfo.GetLeaveStatus(fromdate);
			
			System.out.println("Status of your leave application : " +leaveStatus.toString());
			
			
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("\n Something went wrong!!!! : " + e.getMessage());
		}
	}
	
	
	private static void GenerateEmployeeData() {
		
		Employee emp1 = new Employee();
		emp1.setEmpID(1L);
		emp1.setLeavesRemainig(10);	
		emps.add(emp1);
		
		Employee emp2 = new Employee();
		emp1.setEmpID(2L);
		emp1.setLeavesRemainig(15);	
		emps.add(emp2);
		
		Employee emp3 = new Employee();
		emp1.setEmpID(3L);
		emp1.setLeavesRemainig(8);	
		emps.add(emp3);
		
		Employee emp4 = new Employee();
		emp1.setEmpID(4L);
		emp1.setLeavesRemainig(20);	
		emps.add(emp4);
		
		Employee emp5 = new Employee();
		emp1.setEmpID(5L);
		emp1.setLeavesRemainig(5);	
		emps.add(emp5);
		
	}

}
