package com.AutoLeave;

import java.text.*;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

enum LeaveStatus { Approved, Pending, Rejected};

public class LeaveInfo {
	
	public LeaveStatus LeaveStatus;
	
	public LeaveStatus GetLeaveStatus(Date fromDate) {
		
		try
		{
			SimpleDateFormat df = new SimpleDateFormat("DD/MM/YYYY");
			String dateobj = new Date().toString();
			String dateToday = df.format(dateobj);
			Date today = df.parse(dateToday);
			System.out.println(today.toString());
			
			long difference_In_Time 
	        = fromDate.getTime() - today.getTime(); 
			
			long difference_In_Days 
            = (difference_In_Time 
               / (1000 * 60 * 60 * 24)) 
              % 365;
			
			if(difference_In_Days >= 7L)
			{
				LeaveStatus = LeaveStatus.Approved;
			}
			else {
				
				LeaveStatus = LeaveStatus.Rejected;
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		return LeaveStatus;
	}
	
	public Long CalculateTotalNumberOfDays(Date fromDate, Date toDate) 
	{
		
		Long days = 0L;
		
	    Calendar cal1 = Calendar.getInstance();
	    Calendar cal2 = Calendar.getInstance();
	    cal1.setTime(fromDate);
	    cal2.setTime(toDate);

	    while (cal1.before(cal2)) {
	        if ((Calendar.SATURDAY != cal1.get(Calendar.DAY_OF_WEEK))
	           &&(Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
	        	days++;
	        }
	        cal1.add(Calendar.DATE,1);
	    }
		
		return days;
	}
	
}
